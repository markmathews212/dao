import pytest
from .. import dataAccess

test_binary_data = (
    b'"data": {}',
    b'"data": {"instrumentName": ""}',
    b'"data": {"price": 0.0}'
)

test_dictionaries = (
    {},
    {"instrumentName": ""},
    {"price": 0.0},
)

test_data = [(input, expected) for input, expected
             in zip(test_binary_data, test_dictionaries)]


@pytest.mark.parametrize("input, expected", test_data)
def test_get_data(input, expected):
    assert dataAccess.getData(input) == expected
