from flask import Flask, Response, request
from flask_cors import CORS
# import mysql.connector
from flask_mysqldb import MySQL
import json
import requests


# Server from which to receive data - OCP
# DATAGEN_IP = '192.168.99.100'  # Docker container
# DATAGEN_IP = 'localhost'  # Localhost
DATAGEN_HOST = 'datagen-1'  # OCP
DATAGEN_PORT = '8080'
# DATA_URL = f'{DATAGEN_HOST}:{DATAGEN_PORT}'
DATA_URL = 'http://datagen-1-blackjackroot.apps.dbgrads-6eec.openshiftworkshop.com/'

# # Database details - OCP
# DB_HOST = "10.1.12.85"
# DB_USER = "mysql"
# DB_PASSWORD = "password"
# DB_PORT = '3306'
# DB_NAME = "bjamysql"

# Front-end details - OCP
FRONTEND_HOST = 'frontend'
FRONTEND_PORT = '8080'
FRONTEND_URL = f'{FRONTEND_HOST}:{FRONTEND_PORT}'


app = Flask(__name__)
CORS(app)

# mydb = None
# db_err_message = ""
# try:
#     # mydb = mysql.connector.connect(
#     #     host=DB_HOST,
#     #     user=DB_USER,
#     #     database=DB_NAME,
#     #     password=DB_PASSWORD,
#     #     port=DB_PORT
#     # )
#     app.config['MYSQL_HOST'] = DB_HOST
#     # app.config['MYSQL_PORT'] = DB_PORT
#     app.config['MYSQL_USER'] = DB_USER
#     app.config['MYSQL_PASSWORD'] = DB_PASSWORD
#     app.config['MYSQL_DB'] = DB_NAME
#     mysql = MySQL(app)
#     mydb = mysql.connection
#     print("Debug messages:")
#     print(mysql)
#     print(mydb)
# # except mysql.connector.Error as err:
# #     db_err_message = f"MySQL connector startup error: {err}\n"
# except Exception as err:
#     db_err_message = f"Unknown database startup error: {err}\n"


def getData(line):
    """Convert incoming data into dictionary"""
    line_str = line.decode('ascii')
    line_str = "{" + line_str + "}"  # make it a proper dict
    data = json.loads(line_str)["data"]
    return data


# # Insert streaming data into the database
# def insertLiveData(jSON_row):
#     try:
#         instrument = jSON_row["instrumentName"]
#         counterparty = jSON_row["cpty"]
#         price = jSON_row["price"]
#         type_BS = jSON_row["type"]
#         quantity = jSON_row["quantity"]
#         time_deal = jSON_row["time"]
#
#         mycursor = mydb.cursor()
#
#         insert_instrument = "insert into Instrument (inst_name) values (%s)"
#         val = (instrument)
#         mycursor.execute(insert_instrument, val)
#
#         insert_cpt = "insert into Counterparties (count_name) values (%s)"
#         val = (counterparty)
#         mycursor.execute(insert_cpt, val)
#
#         insert_deal = "insert into Deals_history (inst_id, count_id, price, type, quantity)" \
#                       "values ((select count_id from Counterparties where count_name =(%s) )," \
#                       "(select inst_id from Instruments where inst_name =(%s) )," \
#                       "(%s), (%s), (%s), (%s));"
#         val = (counterparty, instrument, price, type_BS, quantity, time_deal)
#         mycursor.execute(insert_deal, val)
#
#         mydb.commit()
#         return f"Inserted trade (that occurred) at time {time_deal}\n"
#     except Exception as exception:
#         return f"Failed to insert live data due to {exception}\n"


# # Check login credentials
# def validateLogin(request_data):
#     try:
#         data = json.loads(request_data.decode('ascii'))
#         email = data["email"]
#         password = data["password"]
#         mycursor = mydb.cursor()
#         insert_instrument = "select * from login_details where email_address =(%s) " \
#                             "and password =(%s)"
#         val = (email, password)
#         mycursor.execute(insert_instrument, val)
#         result = mycursor.fetchone()
#         return result
#     except Exception as exception:
#         return f"Failed to validate login details due to {exception}\n"

# --------------- Page functions --------------- #

# Handle login requests
# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     try:
#         if request.method == "POST":
#             # return "Received POST request"
#             if mydb is not None and mydb.is_connected():
#                 # login_result = validateLogin(request.data)
#                 login_result = True
#                 if login_result:
#                     return Response(status=200, mimetype='application/json')
#                 else:
#                     return Response(status=401, mimetype='application/json')
#             else:
#                 return "Failed to initialise DB or DB not connected\n"
#         elif request.method == "GET":
#             return "GET request handling not yet implemented\n"
#         else:
#             return f"Cannot handle request method: {request.method}\n"
#     except Exception as err:
#         return f"Unknown error: {err}\n"


@app.route('/login', methods=['GET', 'POST'])
def login():
    try:
        if request.method == "POST":
            def sendSSEToReact():
                while True:
                    data = requests.get(DATA_URL, stream=True)
                    for line in data.iter_lines(chunk_size=1):
                        processed_data = "{" + line.decode('ascii') + "}"
                        yield processed_data  # For SSE, have to return a string object
            return Response(sendSSEToReact(), status=200, mimetype='text/event-stream')
        elif request.method == "GET":
            return "GET request handling not yet implemented\n"
        else:
            return f"Cannot handle request method: {request.method}\n"
    except Exception as err:
        return f"Unknown error: {err}\n"


# # Receive and store incoming data
# @app.route('/read')
# def read():
#     def dataStream():
#         try:
#             data = requests.get(DATA_URL, stream=True)
#             try:
#                 for line in data.iter_lines(chunk_size=1):
#                     if line:
#                         try:
#                             if mydb is not None and mydb.is_connected():
#                                 try:
#                                     data_insertion_result = insertLiveData(getData(line))
#                                     yield data_insertion_result
#                                 except Exception as e:
#                                     yield f"Data insertion failed, {e}\n"
#                             elif mydb is None:
#                                 yield f"Failed to initialise DB - {db_err_message} ; {vars(mysql.app)}\n"
#                             else:
#                                 yield "DB not connected\n"
#                         except Exception as e:
#                             yield f"Failed to insert data: {e}\n"
#             except Exception as e:
#                 yield f"Got data from datagen, rest failed: {e}\n"
#         except Exception as e:
#             yield "Error : {}\n".format(e)
#     return Response(dataStream(), status=200, mimetype='text/event-stream')


# @app.route('/send')
def send():
    def sendDataStream():
        data = requests.get(DATA_URL, stream=True)
        for line in data.iter_lines(chunk_size=1):
            # make POST request to front-end
            payload = getData(line)
            requests.post(FRONTEND_URL, json=payload)
            yield line  # show live data on page
    return Response(sendDataStream(), status=200, mimetype='text/event-stream')


# Retrieve historical data
@app.route('/histdata')
def histData():
    return "Historical trade data extraction has not yet been implemented\n"


@app.route('/teststr')
def teststr():
    return "This is a test"


@app.route('/')
def root():
    return "You are at the root page. Go to /read , /login or /histdata"


def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'), debug=True)


if __name__ == '__main__':
    bootapp()
